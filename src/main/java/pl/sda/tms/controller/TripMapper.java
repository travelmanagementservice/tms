package pl.sda.tms.controller;

import org.mapstruct.Mapper;
import pl.sda.tms.dto.TripForm;
import pl.sda.tms.entity.Trip;
@Mapper
public interface TripMapper {

    Trip toTrip(TripForm tripForm);

    TripForm toTripForm(Trip trip);

}
