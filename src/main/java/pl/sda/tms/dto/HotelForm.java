package pl.sda.tms.dto;
import lombok.Builder;
import lombok.Data;
import java.time.LocalDateTime;

@Data
@Builder
public class HotelForm {

    private String name;
    private AddressForm address;
    private LocalDateTime checkInDate;
    private LocalDateTime checkOutDate;
    private Integer guestNumber;
}
