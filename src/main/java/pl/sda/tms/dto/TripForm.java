package pl.sda.tms.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import pl.sda.tms.entity.Airport;
import pl.sda.tms.entity.Hotel;
import java.time.LocalDateTime;


@Data
@Builder
public class TripForm {

    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private Airport fromAirport;
    private Airport toAirport;
    private HotelForm hotel;
    private Double adultPrice;
    private Double childPrice;
    private Integer peopleLimit;
}
