package pl.sda.tms.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TripsByCountry {
    @JsonProperty("country")
    private String country;
    @JsonProperty("value")
    private BigDecimal amount;
}
