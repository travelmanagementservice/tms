package pl.sda.tms.dto;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class AddressForm {

    private String street;
    private String apartmentUnit;
    private String zipCode;
    private String city;
    private String country;
}
