package pl.sda.tms.entity;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.UUID;
import static org.springframework.util.Assert.state;

@Entity
@Table(name = "contacts")
@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)

public final class Contact {
    @Id
    private UUID id;
    private String phoneNumber;
    private String email;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private Address address;

    public Contact(String phoneNumber, String email, Address address) {

        state(phoneNumber.matches("\\d{9}"), "Phone number is incorrect.");
        state(email.matches("[\\w-\\.]+@([\\w-]+\\.)+[a-z]{2,4}"), "E-mail address is incorrect.");

        this.id = UUID.randomUUID();
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.address = address;
    }
}

