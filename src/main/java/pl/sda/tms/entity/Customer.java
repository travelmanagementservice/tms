package pl.sda.tms.entity;


import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.util.UUID;

import static org.springframework.util.Assert.hasText;
import static org.springframework.util.Assert.notNull;

@Entity
@Table(name = "customers")
@NoArgsConstructor(access = AccessLevel.PRIVATE) // supports jpa/hibernate
@Getter
@EqualsAndHashCode
public final class Customer {

    @Id
    private UUID id;
    private String firstName;
    private String lastName;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "contact_id")
    private Contact contact;

    public Customer(String firstName, String lastName, Contact contact) {
        hasText(firstName, "First name is missing");
        notNull(firstName, "First name cannot be empty");
        hasText(firstName, "Last name is missing");
        notNull(firstName, "First name cannot be empty");

        this.id = UUID.randomUUID();
        this.firstName = firstName;
        this.lastName = lastName;
        this.contact = contact;
    }
}
