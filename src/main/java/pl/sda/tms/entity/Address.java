package pl.sda.tms.entity;

import static org.springframework.util.Assert.hasText;
import static org.springframework.util.Assert.state;

import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.*;
import org.springframework.util.Assert;

@Entity
@Data
@Table(name = "addresses")
@NoArgsConstructor (access = AccessLevel.PRIVATE) // supports jpa/hibernate

public class Address {

    @Id
    private UUID id;
    private String street;
    private String apartmentUnit;
    private String zipCode;
    private String city;
    private String country;

    public Address(String street, String apartmentUnit, String zipCode, String city, String country) {
        hasText(street, "street is empty text");
        state(zipCode.matches("\\d{2}-\\d{3}"), "zip code is invalid");
        hasText(city, "city is empty text");

        this.id = UUID.randomUUID();
        this.street = street;
        this.apartmentUnit = apartmentUnit;
        this.zipCode = zipCode;
        this.city = city;
        this.country = country;
    }
}
