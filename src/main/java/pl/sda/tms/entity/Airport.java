package pl.sda.tms.entity;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

import lombok.NoArgsConstructor;
import static org.springframework.util.Assert.state;

@Entity
@Table(name = "airports")
@NoArgsConstructor(access = AccessLevel.PRIVATE) // supports jpa/hibernate
@Getter
@EqualsAndHashCode
public final class Airport {

    @Id
    private UUID id;
    private String airportCode;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address")
    private Address address;


    public Airport(String airportCode, Address address) {
        state(airportCode.matches("[A-Z]{3}"), "Airport is invalid");
        this.id = UUID.randomUUID();
        this.airportCode = airportCode;
        this.address = address;
    }

}
