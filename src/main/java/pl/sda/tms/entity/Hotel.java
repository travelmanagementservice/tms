package pl.sda.tms.entity;

import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

import static org.springframework.util.Assert.hasText;
import static org.springframework.util.Assert.state;

@Entity
@Getter
@Table(name = "hotel")
@NoArgsConstructor(access = AccessLevel.PRIVATE) // supports jpa/hibernate
@EqualsAndHashCode
public class Hotel  {
    @Id
    private UUID id;
    private String name;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private Address address;
    private LocalDateTime checkInDate;
    private LocalDateTime checkOutDate;
    @NotNull
    private Integer guestNumber;

    public Hotel(String name, Address address, LocalDateTime checkInDate, LocalDateTime checkOutDate, Integer guestNumber) {
        String stringGuestNumber = String.valueOf(guestNumber);
        hasText(name, "Name is invalid");
        state(stringGuestNumber.matches ("[\\d]"), "The max number of guests can be 9");
        this.id = UUID.randomUUID();
        this.name = name;
        this.address = address;
        this.checkInDate = checkInDate;
        this.checkOutDate = checkOutDate;
        this.guestNumber = guestNumber;
    }
}

