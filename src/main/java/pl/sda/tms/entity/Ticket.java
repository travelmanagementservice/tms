package pl.sda.tms.entity;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.UUID;

import static org.springframework.util.Assert.*;

@Entity
@Table(name = "tickets")
@NoArgsConstructor(access = AccessLevel.PRIVATE) // supports jpa/hibernate
@Getter
@EqualsAndHashCode
public final class Ticket {

    @Id
    private UUID id;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "trip_id")
    private Trip trip;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_id")
    private Customer buyer;
    private Double totalPrice;
    private Integer adult;
    private Integer child;

    public Ticket(Trip trip, Customer buyer, Double totalPrice, Integer adult, Integer child) {
        notNull(totalPrice,"The total price cannot be empty");
        notNull(adult, "Adult cannot be empty");
        notNull(child, "Child cannot be empty");

        this.id = UUID.randomUUID();
        this.trip = trip;
        this.buyer = buyer;
        this.totalPrice = totalPrice;
        this.adult = adult;
        this.child = child;
    }


}



