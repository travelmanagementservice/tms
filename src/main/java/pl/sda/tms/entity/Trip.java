package pl.sda.tms.entity;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

import static org.springframework.util.Assert.notNull;
import static org.springframework.util.Assert.state;


@Entity
@Table(name = "trips")
@NoArgsConstructor(access = AccessLevel.PRIVATE) // supports jpa/hibernate
@Getter
@EqualsAndHashCode
public class Trip {

    @Id
    private UUID id;
    @DateTimeFormat
    private LocalDateTime startDate;
    @DateTimeFormat
    private LocalDateTime endDate;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fromAirport_id")
    private Airport fromAirport;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "toAirport_id")
    private Airport toAirport;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "hotel_id")
    private Hotel hotel;
    @NonNull
    private Double adultPrice;
    @NonNull
    private Double childPrice;
    @NonNull
    private Integer peopleLimit;

    public Trip(LocalDateTime startDate, LocalDateTime endDate, Airport fromAirport, Airport toAirport,
                Hotel hotel, Double adultPrice, Double childPrice, Integer peopleLimit) {
        String stringPeopleLimit = String.valueOf(peopleLimit);
        notNull(adultPrice, "The adult price cannot be empty");
        notNull(childPrice, "The adult price cannot be empty");
        state(stringPeopleLimit.matches ("[\\d]"), "The limit of people is 9, yours is invalid");
        this.id = UUID.randomUUID();
        this.startDate = startDate;
        this.endDate = endDate;
        this.fromAirport = fromAirport;
        this.toAirport = toAirport;
        this.hotel = hotel;
        this.adultPrice = adultPrice;
        this.childPrice = childPrice;
        this.peopleLimit = peopleLimit;
    }




}
