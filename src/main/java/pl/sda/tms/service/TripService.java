package pl.sda.tms.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sda.tms.controller.TripMapper;
import pl.sda.tms.dto.TripForm;
import pl.sda.tms.entity.TripRepository;

import javax.transaction.Transactional;
@Service
@RequiredArgsConstructor
public class TripService {

    private final TripRepository tripRepository;

    private final TripMapper tripMapper;

    @Transactional
    public void save(TripForm tripForm) {
        tripRepository.save(tripMapper.toTrip(tripForm));

    }
}
