package pl.sda.tms.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sda.tms.dto.TripsByDate;
import pl.sda.tms.entity.TripRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static java.time.temporal.TemporalAdjusters.firstDayOfMonth;
import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
public class TripsService {

    private final TripRepository tripRepository;

    public List<TripsByDate> getTripsByDate(String countryCode, int year, int month) {
        final LocalDateTime initial = LocalDateTime.now()
                .withYear(year)
                .withMonth(month);
        final LocalDateTime start = initial.with(firstDayOfMonth());
        final LocalDateTime end = initial.with(lastDayOfMonth());
        return tripRepository.findAll(withFilter(ExpensesFilter.builder()
                .user(user)
                .dateFilter(new DateFilter(start, end))
                .build()))
                .stream()
                .collect(groupingBy(el -> el.getId().getDate().toLocalDate(), toList()))
                .entrySet()
                .stream()
                .map(entry -> new TripsByDate(entry.getKey(), entry.getValue()
                        .stream()
                        .map(el -> el.getAmount())
                        .reduce(BigDecimal.ZERO, BigDecimal::add)))
                .collect(toList());
    }
}
