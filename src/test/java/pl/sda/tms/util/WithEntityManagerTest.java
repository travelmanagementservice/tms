package pl.sda.tms.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@SpringBootTest
@Transactional
public class WithEntityManagerTest {
    @Autowired
    protected EntityManager em;

    protected void persist(Object entity) {
        em.persist(entity);
        em.flush();
        em.clear();
    }
}
