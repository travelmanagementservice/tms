package pl.sda.tms.entity;

import org.junit.jupiter.api.Test;
import pl.sda.tms.util.WithEntityManagerTest;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class TripTest extends WithEntityManagerTest {
    @Test
    void shouldSaveTripInDatabase() {
        // given
        final var hotel = new Hotel("Royal Hotel",
                new Address("Blue", "60A", "00-123", "Gdansk", "Poland"),
                LocalDateTime.of(2021,10,20,12,30),
                LocalDateTime.of(2021,10,30,12,30), 9);
        final var airport = new Airport("WAW",
                new Address("Zwirki i Wigury", "1",
                        "00-001", "Warsaw", "Poland"));
        final var trip = new Trip(LocalDateTime.of(2021,10,20,12,30),
                LocalDateTime.of(2021,10,30,12,30),
                airport, airport, hotel, 1200.32, 650.00, 3);
        // when
        persist(trip);
        // then
        final var readTrip = em.find(Trip.class, trip.getId());
        assertEquals(trip, readTrip);

    }
}