package pl.sda.tms.entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import pl.sda.tms.util.WithEntityManagerTest;

class AirportTest extends WithEntityManagerTest {


    @Test
    void shouldSaveAirportInDatabase() {
        // given
        final var airport = new Airport("WAW", new Address("Zwirki i Wigury", "1", "00-001", "Warsaw", "Poland"));

        // when
        persist(airport);

        // then
        final var readAirport = em.find(Airport.class, airport.getId());
        assertEquals(airport, readAirport);
    }


}