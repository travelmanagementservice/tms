package pl.sda.tms.entity;

import org.junit.jupiter.api.Test;
import pl.sda.tms.util.WithEntityManagerTest;

import static org.junit.jupiter.api.Assertions.*;

class CustomerTest extends WithEntityManagerTest {
    @Test
    void shouldSaveCustomerInDatabase() {

        // given
        final var contact = new Contact("070072772", "przyklad@mail.pl", new Address("Zwirki i Wigury", "1", "00-001", "Warsaw", "Poland"));
        final var customer = new Customer("Jacek", "Kamis", contact);

        // when
        persist(customer);

        // then
        final var readCustomer = em.find(Customer.class, customer.getId());
        assertEquals(customer, readCustomer);
    }
}