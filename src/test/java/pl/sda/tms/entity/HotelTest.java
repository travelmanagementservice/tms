package pl.sda.tms.entity;

import org.junit.jupiter.api.Test;
import pl.sda.tms.util.WithEntityManagerTest;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HotelTest extends WithEntityManagerTest {

    @Test
    void shouldSaveHotel(){
        //given
        final Hotel hotel = new Hotel("Royal Hotel",
                new Address("Blue", "60A", "00-123", "Gdansk", "Poland"),
                LocalDateTime.of(2021,10,20,12,30),
                LocalDateTime.of(2021,10,30,12,30), 1);

        // when
        persist(hotel);

        // then
        final var readHotel = em.find(Hotel.class, hotel.getId());
        assertEquals(hotel.getCheckInDate(),readHotel.getCheckInDate());
        assertEquals(hotel, readHotel);
    }

}