package pl.sda.tms.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import pl.sda.tms.util.WithEntityManagerTest;


class AddressTest extends WithEntityManagerTest {

    @Test
    void shouldSaveAddressInDatabase() {
        // given
        final var address = new Address("Lisciasta", "60a", "91-357", "Lodz", "Poland");

        // when
        persist(address);

        // then
        final var readAddress = em.find(Address.class, address.getId());
        assertEquals(address, readAddress);
    }
}