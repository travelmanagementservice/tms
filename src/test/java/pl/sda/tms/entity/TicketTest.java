package pl.sda.tms.entity;

import org.junit.jupiter.api.Test;
import pl.sda.tms.util.WithEntityManagerTest;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class TicketTest extends WithEntityManagerTest {
    @Test
    void shouldSaveTicketInDatabase() {
        // given
        final var address = new Address("Blue", "60A",
                "00-123", "Gdansk", "Poland");
        final var hotel = new Hotel("Royal Hotel", address,
                LocalDateTime.of(2021, 10, 20, 12, 30),
                LocalDateTime.of(2021, 10, 30, 12, 30), 9);
        final var airport = new Airport("WAW",
                new Address("Zwirki i Wigury", "1",
                        "00-001", "Warsaw", "Poland"));
        final var trip = new Trip(LocalDateTime.of(2021, 10, 20, 12, 20),
                LocalDateTime.of(2021, 10, 20, 12, 30),
                airport, airport, hotel, 1300.50, 650.00, 9);
        final var buyer = new Customer("Tom", "Right",
                new Contact("123456789", "tom@gmail.com", address));

        final var ticket = new Ticket(trip, buyer, 1500.99, 30, 3);

        // when
        persist(ticket);
        // then
        final var readTicket = em.find(Ticket.class, ticket.getId());
        assertEquals(ticket, readTicket);
    }

}