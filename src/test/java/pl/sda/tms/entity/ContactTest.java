package pl.sda.tms.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import pl.sda.tms.util.WithEntityManagerTest;

class ContactTest  extends WithEntityManagerTest {
    
    @Test
    void shouldSaveContactInDatabase(){

        final var contact = new Contact("070072772", "przyklad@mail.pl", new Address("Zwirki i Wigury", "1", "00-001", "Warsaw", "Poland"));
        
        persist(contact);

        final var readContact = em.find(Contact.class, contact.getId());

        assertEquals(contact, readContact);
    }

}